class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :template_name
      t.string :slug
      t.jsonb :config
      t.timestamps null: false
    end
  end
end
