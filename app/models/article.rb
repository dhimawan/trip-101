class Article < ActiveRecord::Base

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  # Try building a slug based on the following fields in
  # increasing order of specificity.
  def slug_candidates
    [
      :title
    ]
  end

  def self.populate_articles
    Article.populate(1000) do |article|
      article.title = Faker::Book.title
      article.content = Faker::Hipster.paragraphs(1, true)
    end
  end

end
