class Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  after_save :reload_routes

  def reload_routes
    DynamicRouter.reload
  end
  
  def slug_candidates
    [
      :template_name
    ]
  end
end
