module ApplicationHelper
  
  def generate_content(page)
    data = case page["type"]
      when "articles"
        Article.find(page["article_ids"])
      else
        "page unavailable"
      end

    # return data
    render :partial => "shareds/articles/#{page["layout"]}", :locals => {:data => data}

  end

end
