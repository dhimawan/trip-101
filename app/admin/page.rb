ActiveAdmin.register Page do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :template_name,:slug,:config

hstore_editor

form do |f|
  f.inputs do
    f.input :template_name
    # f.input :slug
    # f.input :config, as: :text
    f.input :config, as: :hstore
  end
  f.actions
end


end
