class PagesController < ApplicationController

  def show 
    @page = Page.friendly.find(params[:id])
    redirect_to not_found_path unless @page
  end 

end
